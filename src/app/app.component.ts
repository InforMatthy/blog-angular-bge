import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  posts = [
      {
          title: 'Mon premier Post',
          content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed rhoncus nunc neque, non dictum urna accumsan ut. In facilisis risus dolor, eu aliquet turpis tempus sed. Sed a enim porttitor, iaculis mauris sed, consectetur ligula. Integer tempor nibh nisi, a suscipit sem pulvinar nec. Sed tincidunt dui ut ipsum laoreet fringilla. Aliquam rutrum urna velit, vitae laoreet felis dictum at. Suspendisse eget accumsan neque, eget pharetra tortor. Duis tempor malesuada mauris eu viverra. Sed quis vehicula erat, eget posuere sem. Quisque vel iaculis purus. Donec ut ligula id neque vestibulum tempus. Sed vel placerat felis, sit amet accumsan enim. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Quisque euismod vitae nibh id eleifend. Aliquam cursus viverra mauris, id viverra tortor venenatis ac.',
          loveIts: 2,
          hateIts: 0,
          created_at: new Date('2018-08-24')
      },
      {
          title: 'Mon second Post',
          content: 'Maecenas ac libero sed massa vulputate varius. Donec non quam ultrices, iaculis nunc in, aliquam libero. Integer vel magna ipsum. Nunc a fermentum turpis, posuere pretium magna. Proin fringilla blandit nisl blandit condimentum. Sed posuere erat vitae orci tempus accumsan. Nam fermentum lacus ut est pulvinar, ut viverra augue vehicula. Curabitur est ante, consectetur a nulla eu, cursus sodales nisl. In hac habitasse platea dictumst.',
          loveIts: 0,
          hateIts: 1,
          created_at: new Date('2018-09-12')
      },
      {
          title: 'Mon dernier Post',
          content: 'Curabitur varius hendrerit erat sodales commodo. Integer luctus, massa vitae vehicula sagittis, dolor sapien semper urna, eget convallis nisi ipsum sed augue. Maecenas sit amet elit id dolor aliquet placerat id non turpis. Nam sed turpis placerat, suscipit sapien sit amet, facilisis augue. Nam eu sapien fringilla, rhoncus lectus sit amet, tincidunt quam. Morbi id est nulla. Donec laoreet commodo lobortis. Maecenas eu venenatis tortor. Mauris magna tortor, sagittis sodales magna non, scelerisque porttitor augue. Sed hendrerit consectetur est vitae porta. Nam tristique vehicula lorem, ut maximus lorem. Nulla dapibus elit sapien, vel viverra nibh aliquam eu. Duis consequat, enim ac tincidunt tempus, arcu lorem feugiat justo, in sollicitudin erat orci vitae neque. Aenean sollicitudin lectus id nisi tincidunt tristique. Nam vitae justo vel mi rutrum tempor. Ut ac volutpat orci, in vestibulum justo.',
          loveIts: 0,
          hateIts: 0,
          created_at: new Date('2018-09-24')
      }
  ]
  constructor() {}
}
